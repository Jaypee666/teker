<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PasswordReset extends Model
{
	protected $table = 'password_resets';

    static public function createPasswordReset($request, $code) {
        $reset = new PasswordReset();
        $reset->email = $request->email;
        $reset->code = $code;
        $reset->status = 'Live';
        $reset->save();
    }

    static public function checkCode($request) {
        $code = PasswordReset::where('code', $request->code)->first();
        if($code) {
        	$response = [
                'email' => $code->email,
                'code' => 200,
            ];
        } else {
        	$response = [
                'message' => 'Wrong code!',
                'code' => 400,
            ];
        }

        return $response;
    }
}
