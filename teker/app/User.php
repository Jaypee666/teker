<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    static public function createUser ($request) {
        $user = new User();
        $user->name = $request->name;
        $user->username = $request->username;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        if($user->save()){
            $response = [
                'message' => 'Registration Succesful!',
                'code' => 200,
            ];
        } else {
            $response = [
                'message' => 'Registration Failed! Please try again.',
                'code' => 500,
            ];
        }
        return $response;
    }

    static public function resetPassword($request, $email) {
        $user = User::where('email', $email)->get();
        $user->password = bcrypt($request->password);
        if($user->save()){
            $response = [
                'message' => 'Password Reset Succesful!',
                'code' => 200,
            ];
        } else {
            $response = [
                'message' => 'Password Reset Failed! Please try again.',
                'code' => 500,
            ];
        }
        return $response;
    }

}
