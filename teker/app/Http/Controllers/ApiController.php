<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\Validator;
use App\User;
use App\PasswordReset;
use Auth;

class ApiController extends Controller
{
    public function loginUser(Request $request)
    {
    	$credentials = $request->only('username', 'password');
    	if (Auth::attempt($credentials)) {
            return '[{"message":"Login Succesful!", "code":"200"}]';
        } else {
        	return '[{"message":"Wrong email or password.", "code":"400"}]';
        }
    }

    public function registerUser(Request $request)
    {
    	$validator = Validator::make($request->all(), [
            'username' => 'required|unique:users',
            'email' => 'required|email|unique:users',
            'name' => 'required|string|max:50',
            'password' => 'required'
       ]);

    	if($validator->fails()){
    		$message = $validator->messages()->first();
    		return '[{"message":"'.$message.'", "code":"400"}]';
    	} else {
    		$response = User::createUser($request);
    		return '[{"message":"'.$response['message'].'", "code":"'.$response['code'].'"}]';
    	}
    }

    public function checkEmail(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|unique:users'
        ]);

        if($validator->fails()){
            $message = $validator->messages()->first();
            return '[{"message":"'.$message.'", "code":"400"}]';
        } else {
            $code = str_random(4);
            PasswordReset::createPasswordReset($request, $code);
            $to = $request->email;
            $subject = "Password Reset Code!";
            $txt = "Your code is ".$code;
            $headers = "From: nonimousgaming@gmail.com";

            mail($to,$subject,$txt,$headers);
            return '[{"message":"An email has been sent to your email address with your password reset code.", "code":"200"}]';
        }
    }

    public function resetPassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'code' => 'required',
            'password' => 'required'
        ]);

        if($validator->fails()){
            $message = $validator->messages()->first();
            return '[{"message":"'.$message.'", "code":"400"}]';
        } else {
            $checkCode = PasswordReset::checkCode($request);
            if($checkCode['code'] == 200) {
                $response = User::resetPassword($request, $checkCode['email']);
                return '[{"message":"'.$response['message'].'", "code":"'.$response['code'].'"}]';
            } else {
                return '[{"message":"'.$checkCode['message'].'", "code":"'.$checkCode['code'].'"}]';
            }
        }
    }
}
